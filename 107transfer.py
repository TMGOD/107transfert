#!/usr/bin/env -S python3
import sys

def help():
    print("""USAGE
./107transfer [num den]+
DESCRIPTION
num polynomial numerator defined by its coefficients
den polynomial denominator defined by its coefficients""")
    return


def parse_arg():
    try:
        if len(sys.argv) == 2 and (sys.argv[1] == "-h") and len(sys.argv[1]) == 2:
            help()
            exit(0)
        if (len(sys.argv) == 1) or (len(sys.argv) % 2 == 0):
            sys.exit(84)
            return
        for i in sys.argv[1:]:
            if len(i) == 0:
                sys.exit(84)
            for e in range(len(i)):
                if i[0] == '*':
                    sys.exit(84)
                if i[len(i) - 1] == '*':
                    sys.exit(84)
                if (i[e].isalpha()) and (i[e] != '*'):
                    sys.exit(84)
                if (i[e] == '*') and (i[e + 1] == '*'):
                    sys.exit(84)

        arg_list = []
        for i in sys.argv[1:]:
            sub_list = [int(index) for index in i.split('*')]
            arg_list.append(sub_list)
        return arg_list
    except:
        sys.exit(84)


def div(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        sys.exit(84)


def calculation_process(operand, start, arg_list, i):
    calcul_operand = []
    operand = arg_list[i]
    for index in range(0, len(operand), 1):
        calcul_operand.append((operand[index] * pow(start, index)))
    calcul_operand = sum(calcul_operand)
    return calcul_operand


def main():
    arg_list = parse_arg()
    start = 0.0
    step = 0.001
    stop = 1.0

    numerator = []
    denumerator = []
    for i in range(len(arg_list)):
        if i % 2 == 0:
            numerator.append(arg_list[i])
        else:
            denumerator.append(arg_list[i])

    while (start < (stop + step)):
        for i in range(0, len(sys.argv[1:]), 2):
            calcul_numerator = calculation_process(numerator, start, arg_list, i)
            calcul_denumerator = calculation_process(denumerator, start, arg_list, i + 1)
            stop = stop * div(calcul_numerator, calcul_denumerator)
        print(f"{round(start, 3):.3f} -> {stop:.5f}")
        start += step
        stop = 1.0
    return 0


if __name__ == '__main__':
    main()