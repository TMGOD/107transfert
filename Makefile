##
## EPITECH PROJECT, 2023
## B-MAT-100-RUN-1-1-105torus-hugo.payet
## File description:
## Makefile
##

SRC = 107transfer.py

TEST = test.py

all	:
	cp $(SRC) 107transfer

tests_run :
	./tests/$(TEST)

clean	:
	rm -f 107transfer